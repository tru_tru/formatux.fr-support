= Commandes de bases
ifndef::doctype[]
:doctype: article
:encoding: utf-8
:lang: fr
:numbered:
:sectnumlevels: 3
:description: TD Formatux - Les commandes pour utilisateurs Linux
:revnumber: 1.0
:revdate: 09-08-2017
:experimental:
:source-highlighter: highlightjs
:pdf-page-size: A4
:source-language: bash
endif::[]

== ATELIER 1 : Affichage et identification

.icon:mortar-board[] Objectifs
****
icon:check-square-o[] Prendre en main un poste de travail, +
icon:check-square-o[]  Se renseigner sur les utilisateurs connectés.
****

=== Exercice 1.1 : Identification

* Se connecter sur la console 1.

* Afficher les informations concernant le login courant.

[source,html]
----
$
~
---- 

* Se connecter sur la console 2 avec le même utilisateur.

* Afficher les informations concernant le login courant.

[source,bash]
----
$ 
~
----

* Afficher les informations concernant l’utilisateur **patrick**.

[source,bash]
----
$ 
~
----

* D'autres utilisateurs sont-ils connectés sur le serveur ?

[source,bash]
----
$ 
~
~
----

* Depuis quelle console êtes-vous connecté ? 

[source,bash]
----
$
~
----

== ATELIER 2 : A l'aide !

.icon:mortar-board[] Objectifs
****
icon:check-square-o[] Rechercher dans la documentation, +
icon:check-square-o[] Se renseigner sur les utilisateurs connectés.
****

=== Exercices 2.1 : Utiliser le manuel

* Rechercher de l’aide sur la commande `passwd`

[source,bash]
----
$
~
~
~
~
----

[source,bash]
----
$
----

* Rechercher des informations sur le formatage du fichier `passwd`

[source,bash]
----
$
----

* Quel est l’emplacement de l’`UID` dans le fichier `passwd` à l’aide du `man`.

[source,bash]
----
$
~
----

* Vérifier la date du jour.

[source,bash]
----
$
~
----

* Effacer la console.
[source,bash]
----
$
----

== ATELIER 3 : Arborescence et fichiers

.icon:mortar-board[] Objectifs
**** 
icon:check-square-o[] créer, supprimer, déplacer des fichiers ou des répertoires ; +
icon:check-square-o[] se déplacer dans l’arborescence.
****

=== Exercice 3.1 : Création de répertoires

* Afficher le répertoire courant.

[source,bash]
----
$
~
----

* Se déplacer de deux façons différentes sous le répertoire `/home`.

icon:check[] chemin absolu : 
[source,bash]
----
$
----

icon:check[] chemin relatif : 

[source,bash]
----
$
----

* Vérifier que `/home` soit bien le nouveau répertoire courant.

[source,bash]
----
$
~
----

* Retourner dans le répertoire de connexion, et vérifier.

[source,bash]
----
$
$
~
----

* Créer les répertoires suivants : `/home/stagiaire/tickets/` `/home/stagiaire/tickets/pierre/` `/home/stagiaire/tickets/jacques/`

[source,bash]
----
$
$
~
----

=== Exercice 3.2 : Gestion des fichiers

* Créer le fichier `/home/stagiaire/tickets/listing_en_cours`.

[source,bash]
----
$
----

* Copier ce fichier dans les répertoires `/home/stagiaire/tickets/pierre` et `/home/stagiaire/tickets/jacques`. Vérifier la taille de ces fichiers.

[source,bash]
----
$
$
----

icon:check[] Vérifier la copie en comparant les tailles :

[source,bash]
----
$
~
~
~
~
~
----

* Renommer le fichier `/home/stagiaire/tickets/jacques/listing_en_cours` en `listing_fini`.

[source,bash]
----
$
~
----

* Déplacer et renommer le fichier `/home/stagiaire/listing_en_cours` en `/STAGE/commandes/archive_listing`.


[source,bash]
----
$
$
~
----

=== Exercice 3.3 : Gestion des répertoires

* Copier le répertoire `/home/stagiaire/tickets/pierre/` et son contenu en le renommant `/home/stagiaire/tickets/sauvegarde`.

[source,bash]
----
$
----

* Renommer le répertoire `/home/stagiaire/tickets/sauvegarde/` en `/home/stagiaire/tickets/archives`.

[source,bash]
----
$
----

* Copier le répertoire `/home/stagiaire/tickets/` dans le répertoire `/STAGE/commandes/`.

[source,bash]
----
$
----

=== Exercice 3.4 : Suppression de fichiers et répertoires

* Afficher le contenu des répertoires `/home/stagiaire/tickets/jacques/` et `/home/stagiaire/tickets/pierre/`.

[source,bash]
----
$
~
~
~
~
~
~
~
----

* Supprimer le répertoire `/home/stagiaire/tickets/jacques/` avec la commande `rmdir`.

[source,bash]
----
$
~
$
$
----

* Supprimer le répertoire `/home/stagiaire/pierre/` en une seule commande.

[source,bash]
----
$
----

== ATELIER 4 : Recherches et filtres

.icon:mortar-board[] Objectifs
****
icon:check-square-o[] rechercher un fichier ; +
icon:check-square-o[] rechercher du texte dans un fichier ; +
icon:check-square-o[] afficher un fichier, trier son contenu.
****

=== Exercice 4.1 : Affichage et filtres

* Copier dans le répertoire de connexion `/home/stagiaire` le fichier `/etc/passwd`. 

[NOTE]
====
Dorénavant, travailler sur cette copie.
====

[source,bash]
----
$
----

* Afficher les 7 premières lignes puis les 3 dernières.

[source,bash]
----
$
$
----

*  Retrouvez la ligne contenant `alain`.

[source,bash]
----
$
~
----

* Trier ce fichier par ordre d'`UID` croissant.

[source,bash]
----
$
----

* Combien y a-t-il d'utilisateurs créés sur le serveur ?

[source,bash]
----
$
~
----

* Déplacer ce fichier dans le répertoire `/STAGE/commandes`.

[source,bash]
----
$
----

* Afficher les fichiers `passwd` présents dans le dossier `/STAGE` en précisant leur type.

[source,bash]
----
$
~
----

== ATELIER 5 : tubes et redirections

.icon:mortar-board[] Objectifs
****
icon:check-square-o[] utiliser un tube ; +
icon:check-square-o[] utiliser une redirection.
****

=== Exercices

* Créer un fichier `/home/stagiaire/suivi_admin`.

[source,bash]
----
$
----

* Se connecter sur le terminal 2 et suivre les modifications du fichier en direct.

[source,bash]
----
$
----

[IMPORTANT]
====
La suite de ce TP se fait sans éditeur de texte !
====

Retourner sous le terminal 1 et ajouter au fichier `suivi_admin` le texte `Voici les répertoires de /STAGE/commandes/gestion/ :`.

[source,bash]
----
$
~
----

* Toujours dans `suivi_admin`, ajouter la liste des répertoires de `/STAGE/commandes/gestion/` en faisant apparaître les tailles avec l’indication Ko, Mo, Go ou To.

[source,bash]
----
$
~
----

* Vérifier le contenu du fichier en basculant sur le terminal 2.

* Retourner sous terminal 1 et ajouter au fichier `suivi_admin` le texte `Voici les personnes ayant un fichier listing_en_cours sous /STAGE/commandes/gestion/ :`.

[source,bash]
----
$
~
~
----

* Tapez la commande :

[source,bash]
----
[stagiaire]$ find /STAGE/commandes/tickets –listing_en_cours >> /home/stagiaire/suivi_admin 2>/home/stagiaire/erreur
----

* Basculer sur le terminal 2 et vérifier que la commande se soit bien exécutée.

* Corriger la commande pour remplir le fichier `suivi_admin`.

[source,bash]
----
$
~
~
----

* Afficher parmi les 3 dernières lignes du fichier `suivi_admin` celles qui contiennent `pierre`.

[source,bash]
----
$
~
----

* Retourner sous le terminal 2 et se déconnecter.

[source,bash]
----
$
~
----