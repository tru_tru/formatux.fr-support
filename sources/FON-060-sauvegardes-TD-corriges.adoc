////
Les supports de Formatux sont publiés sous licence Creative Commons-BY-SA et sous licence Art Libre.
Vous êtes ainsi libre de copier, de diffuser et de transformer librement les œuvres dans le respect des droits de l’auteur.

    BY : Paternité. Vous devez citer le nom de l’auteur original.
    SA : Partage des Conditions Initiales à l’Identique.

Licence Creative Commons-BY-SA : https://creativecommons.org/licenses/by-sa/3.0/fr/
Licence Art Libre : http://artlibre.org/

Auteurs : Patrick Finet, Xavier Sauvignon, Antoine Le Morvan
////

= Sauvegardes et restaurations

== ATELIER 1 : La commande tar

.Objectifs
****
* effectuer une sauvegarde avec l’utilitaire tar ;
* restaurer la sauvegarde.
****

.Prérequis
****
* maîtriser le concept de chemin absolu et de chemin relatif.
****

=== Exercice 1.1 : Sauvegarde en mode absolu de /etc

* Réaliser une sauvegarde en mode absolu de « /etc ». Nommer cette sauvegarde de façon à connaître l’utilitaire utilisé. Elle sera placée dans le répertoire « /Disque1/sauvegardes ».

Vérifier l’existence de l’arborescence « /Disque1/sauvegardes », sinon la créer.

[NOTE]
====
« /Disque1 » est une partition créée durant le TP « Ajout d’un disque ».
====

Afin de respecter la convention de nommage vu en cours, nous noterons « P » quand la sauvegarde est effectuée en mode absolu.

[source,]
----
[root]# mkdir -p /Disque1/sauvegardes
----

Puis créer la sauvegarde en mode absolu :

[source,]
----
[root]# tar cvfP /Disque1/sauvegardes/etc.P.`date +%j`.tar /etc
----

ou 

[source,]
----
[root]# cd /Disque1/sauvegardes
[root]# tar cvfP etc.P.`date +%j`.tar /etc
----

=== Exercice 1.2 : Ajout d’un fichier

* Ajouter le fichier « /usr/bin/passwd » à la sauvegarde précédente.

La sauvegarde précédente ayant été faite en mode absolu, on ajoutera le fichier en mode absolu également :

[source,]
----
[root]# tar rvfP /Disque1/sauvegardes/etc.P.`date +%j`.tar /usr/bin/passwd
----

ou 

[source,]
----
[root]# cd /Disque1/sauvegardes
[root]# tar rvfP etc.P.`date +%j`.tar /usr/bin/passwd
----

=== Exercice 1.3 : Sauvegarde en mode absolu de /home

* Réaliser une sauvegarde en mode absolu de /home. Elle portera un nom explicite. Cette sauvegarde sera placée dans le répertoire « /Disque1/sauvegardes ».

[source,]
----
[root]# tar cvfP /Disque1/sauvegardes/home.P.`date +%j`.tar /home
----

ou 

[source,]
----
[root]# cd /Disque1/sauvegardes
[root]# tar cvfP home.P.`date +%j`.tar /home
----

=== Exercice 1.4 : Sauvegarde compréssée en mode relatif

* Réaliser une sauvegarde compressée en mode relatif de « /etc ». Pour la compression, vous avez le choix de la méthode. Nommer cette sauvegarde de façon à connaître l’utilitaire utilisé. Cette sauvegarde sera placée dans le répertoire « /Disque1/sauvegardes ».

La sauvegarde est à réaliser en mode relatif et compressée (plus besoin de la clé « P »).

* 1ère méthode avec gzip

[source,]
----
[root]# tar cvfz /Disque1/sauvegardes/etc.`date +%j`.tar.gz /etc
----

* 2ème méthode avec bzip2

[source,]
----
[root]# tar cvfj /Disque1/sauvegardes/etc.`date +%j`.tar.bz2 /etc
----

=== Exercice 1.5 : Vérification des sauvegardes

* Lister le contenu des trois sauvegardes réalisées.

Mettre le résultat du listage dans « /Disque1/sauvegardes/ResultatDesSauvegardes ». 

Chaque partie du fichier sera séparée par un commentaire.

[source,]
----
[root]# cd /Disque1/sauvegardes
[root]# echo "Lecture de la sauvegarde etc.P.`date +%j`.tar" >ResultatDesSauvegardes
[root]# echo "----------------------------------" >>ResultatDesSauvegardes
[root]# tar tvfP etc.P.`date +%j`.tar >> ResultatDesSauvegardes
[root]# echo >> ResultatDesSauvegardes
[root]# echo "Lecture de la sauvegarde home.P.`date +%j`.tar" >>ResultatDesSauvegardes
[root]# echo "-----------------------------------" >>ResultatDesSauvegardes
[root]# tar tvfP home.P.`date +%j`.tar >> ResultatDesSauvegardes
[root]# echo >> ResultatDesSauvegardes

[root]# echo "Lecture de la sauvegarde etc.`date +%j`.tar.gz" >>ResultatDesSauvegardes
[root]# echo "-----------------------------------" >>ResultatDesSauvegardes
[root]# tar tvfz etc.`date +%j`.tar.gz >> ResultatDesSauvegardes
[root]# echo >> ResultatDesSauvegardes
----

ou 

[source,]
----
[root]# tar tvfj etc.`date +%j`.tar.bz2 >> ResultatDesSauvegardes
[root]# echo >> ResultatDesSauvegardes
----

=== Exercice 1.6 : Restauration en mode absolu

* Restaurer /etc en mode absolu.

[CAUTION]
====
Souvenez-vous que le fichier « /usr/bin/passwd » a été ajouté à cette sauvegarde. Pour ne restaurer que le répertoire /etc, il faut le préciser dans la commande.
====

[source,]
----
[root]# tar xvfP /Disque1/sauvegardes/etc.P.`date +%j`.tar /etc
----

* Placer la commande utilisée dans le fichier « /Disque1/sauvegardes/CmdSauvegardes ».
Utiliser un commentaire pour spécifier cette commande.

[source,]
----
[root]# echo "Restauration de /etc en mode absolu" > CmdSauvegardes
[root]# echo "-----------------------------------" >> CmdSauvegardes
----

[TIP]
====
À l’aide de la flèche haute du clavier, je recherche ma commande de sauvegarde précédente :
====

[source,]
----
[root]# echo "tar xvfP /Disque1/sauvegardes/etc.P.`date +%j`.tar /etc" >>CmdSauvegardes
----

=== Exercice 1.7 : Restauration en mode relatif

* Restaurer /home/GroupeA en mode relatif.

La sauvegarde du répertoire /home a été réalisée en mode absolu.
Pour la restaurer en mode relatif, il suffit d’omettre la clé « P » lors de la restauration.

[WARNING]
====
Attention, connaître le répertoire courant est important lors d’une restauration en relatif.
====

De plus, comme précédemment, il faut restaurer  seulement une partie de la sauvegarde « /home/GroupeA ». 

Se placer dans un répertoire neutre (ici « /tmp ») pour que « home/GroupeA » soit restauré. Notez bien, que c’est effectivement « home/GroupeA » qui sera restauré et non « /home/GroupeA » car la restauration est en mode relatif !!

[source,]
----
[root]# cd /tmp
----

Notez que le « / » devant « home/GroupeA » est nécessaire car c’est ce que contient effectivement la sauvegarde, qui pour rappel a été réalisée en mode absolu.

[source,]
----
[root]# tar xvf /Disque1/sauvegardes/home.P.`date +%j`.tar /home/GroupeA
----

* Placer la commande utilisée dans le fichier « /Disque1/sauvegardes/CmdSauvegardes ».
Vous utiliserez un commentaire pour spécifier cette commande.

[source,]
----
[root]# echo >> CmdSauvegardes
[root]# echo "Restauration de /home/GroupeA en mode relatif" >> CmdSauvegardes
[root]# echo "-----------------------------------" >> CmdSauvegardes
[root]# echo "tar xvf /Disque1/sauvegardes/home.P.`date +%j`.tar /home/GroupeA" >> CmdSauvegardes
----

=== Exercice 1.8 : Restauration de la sauvegarde compréssée

* Restaurer la sauvegarde compressée.

La sauvegarde compressée ayant été réalisée en mode relatif, nous devons la restaurer dans ce mode. Il est donc impératif de bien se positionner au préalable. Ensuite, il faut penser à utiliser la bonne clé de décompression.

[source,]
----
[root]# cd /tmp
[root]# tar xvfz /Disque1/sauvegardes/etc.`date +%j`.tar.gz
----

ou 

[source,]
----
[root]# tar xvfj /Disque1/sauvegardes/etc.`date +%j`.tar.bz2
----

* Placer la commande utilisée dans le fichier « /Disque1/sauvegardes/CmdSauvegardes ». Vous utiliserez un commentaire pour spécifier cette commande.

[source,]
----
[root]# echo >> CmdSauvegardes
[root]# echo "Restauration de la sauvegarde compressée" >> CmdSauvegardes
[root]# echo "----------------------------------------" >> CmdSauvegardes
[root]# echo "tar xvfz /Disque1/sauvegardes/etc.`date +%j`.tar.gz" >> CmdSauvegardes
----

ou 

[source,]
----
[root]# echo "tar xvfj /Disque1/sauvegardes/etc.`date +%j`.tar.bz2" >> CmdSauvegardes
----

== ATELIER 2 : La commande cpio

.Objectifs
****
* effectuer une sauvegarde avec l’utilitaire cpio ;
* restaurer la sauvegarde.
****

.Prérequis
****
* maîtriser le concept de chemin absolu et de chemin relatif.
****

=== Exercice 2.1 : Sauvegarde en mode absolu

* Réaliser une sauvegarde de « /etc » en mode absolu. Nommer cette sauvegarde de façon à connaître l’utilitaire utilisé. Cette sauvegarde sera placée dans le répertoire « /Disque1/sauvegardes ».

Pour réaliser une sauvegarde en mode absolu, il faut lister le répertoire à sauvegarder en mode absolu. Afin de respecter la convention de nommage vu en cours, noter « A » quand la sauvegarde est effectuée en mode absolu.

[source,]
----
[root]# find /etc | cpio -ov >/Disque1/sauvegardes/etc.A.`date +%j`.cpio
----

ou 

[source,]
----
[root]# find /etc | cpio -ovF /Disque1/sauvegardes/etc.A.`date +%j`.cpio
----

/etc est listé en absolu du fait de la présence du « / »

=== Exercice 2.2 : Sauvegarde en mode relatif avec « cpio »

* Réaliser une sauvegarde en mode relatif de /home/GroupeA.

Elle portera un nom explicite. Cette sauvegarde sera placée dans le répertoire «/Disque1/sauvegardes».

Pour réaliser une sauvegarde en mode relatif, il faut lister le répertoire à sauvegarder en mode relatif et donc il faut se trouver à l’endroit où se trouve le répertoire à sauvegarder.

[source,]
----
[root]# cd /
[root]# find home/GroupeA | cpio -ov >/Disque1/sauvegardes/HomeGroupeA.`date +%j`.cpio
----

ou 

[source,]
----
[root]# find home/GroupeA | cpio -ovF /Disque1/sauvegardes/HomeGroupeA.`date +%j`.cpio
----

home/GroupeA est listé en relative car son chemin ne commence pas par un “/”.

=== Exercice 2.3 : Vérification des sauvegardes

* Lister le contenu des deux sauvegardes réalisées et mettre le résultat du listage dans « /Disque1/sauvegardes/ResultatDesSauvegardes ».

[source,]
----
[root]# cd /Disque1/sauvegardes
[root]# echo >> ResultatDesSauvegardes
[root]# echo "Lecture de la sauvegarde etc.A.`date +%j`.cpio" >>ResultatDesSauvegardes
[root]# echo "-----------------------------------" >>ResultatDesSauvegardes
[root]# cpio -tv <etc.A.`date +%j`.cpio >> ResultatDesSauvegardes
----

ou 

[source,]
----
[root]# cpio -tvF etc.A.`date +%j`.cpio >> ResultatDesSauvegardes

[root]# echo >> ResultatDesSauvegardes
[root]# echo "Lecture de la sauvegarde HomeGroupeA.`date +%j`.cpio" >>ResultatDesSauvegardes
[root]# echo "--------------------------------------" >>ResultatDesSauvegardes
[root]# cpio -tv <HomeGroupeA.`date +%j`.cpio >>ResultatDesSauvegardes
----

ou 

[source,]
----
[root]# cpio -tvF HomeGroupeA.`date +%j`.cpio >>ResultatDesSauvegardes
----

=== Exercice 2.4 : Restauration en mode absolu

* Restaurer « /etc » en mode absolu.

[WARNING]
====
La sauvegarde ayant été réalisée en mode absolu, elle sera restaurée par défaut en mode absolu.
====

L’utilisation de -u permet d’écraser tous les fichiers sans demander le remplacement des fichiers récents par de plus anciens.

[source,]
----
[root]# cpio -iuv <etc.A.`date +%j`.cpio
----

ou 

[source,]
----
[root]# cpio -iuvF etc.A.`date +%j`.cpio
----

* Placer la commande utilisée dans le fichier « /Disque1/sauvegardes/CmdSauvegardes ». Vous utiliserez un commentaire pour spécifier cette commande.

[source,]
----
[root]# echo >> CmdSauvegardes 
[root]# echo "Restauration de la sauvegarde etc.A.`date +%j`.cpio" >>CmdSauvegardes
[root]# echo "----------------------------------------" >> CmdSauvegardes
[root]# echo " cpio -iuv <etc.A.`date +%j`.cpio " >> CmdSauvegardes
----

ou 

[source,]
----
[root]# echo "cpio -iuvF etc.A.`date +%j`.cpio " >> CmdSauvegardes
----

=== Exercice 2.5 : Restauration en mode relatif d’une sauvegarde en mode absolu

* Restaurer « /etc » en mode relatif.

Pour restaurer en mode relatif une sauvegarde ayant été réalisée en mode absolu, il faut préciser l’option « --no-absolute-filenames ».

En général quand une sauvegarde est restaurée en mode relatif, c’est pour la vérifier et donc la restaurer dans un répertoire autre que celui naturellement prévu.

[source,]
----
[root]# cd /tmp
[root]# cpio --no-absolute-filenames -iv </Disque1/sauvegardes/etc.A.`date +%j`.cpio
----

ou 

[source,]
----
[root]# cpio --no-absolute-filenames -ivF /Disque1/sauvegardes/etc.A.`date +%j`.cpio
----

(Notez bien que le nom du fichier de sauvegarde doit se trouver directement après l’option « F », c’est pourquoi l’option « --no-absolute-filenames » est placé en premier).

* Placer la commande utilisée dans le fichier « /Disque1/sauvegardes/CmdSauvegardes ».

Vous utiliserez un commentaire pour spécifier cette commande.

[source,]
----
[root]# cd /Disque1/sauvegardes
[root]# echo >> CmdSauvegardes
[root]# echo "Restauration en relatif de etc.A.`date +%j`.cpio">> CmdSauvegardes
[root]# echo "-------------------------------------">> CmdSauvegardes
[root]# echo "cd /tmp" >> CmdSauvegardes
[root]# echo "cpio --no-absolute-filenames -iv </Disque1/sauvegardes/etc.A.`date +%j`.cpio" >> CmdSauvegardes
----

ou 

[source,]
----
[root]# echo "cpio --no-absolute-filenames -ivF /Disque1/sauvegardes/etc.A.`date +%j`.cpio" >> CmdSauvegardes
----

=== Exercice 2.6 : Restauration en mode relatif

* Restaurer « /home/GroupeA » en mode relatif.

[source,]
----
[root]# cd /tmp
[root]# cpio -idv </Disque1/sauvegardes/HomeGroupeA.`date +%j`.cpio
----

ou 

[source,]
----
[root]# cpio -idvF /Disque1/sauvegardes/HomeGroupeA.`date +%j`.cpio
----

* Placer la commande utilisée dans le fichier « /Disque1/sauvegardes/CmdSauvegardes ».
Vous utiliserez un commentaire pour spécifier cette commande.

[source,]
----
[root]# cd /Disque1/sauvegardes
[root]# echo >> CmdSauvegardes
[root]# echo "Restauration en relatif de HomeGroupeA.`date +%j`.cpio" >>CmdSauvegardes
[root]# echo "-----------------------------------------" >> CmdSauvegardes
[root]# echo "cpio -idv </Disque1/sauvegardes/HomeGroupeA.`date +%j`.cpio" >>CmdSauvegardes
----

ou 

[source,]
----
[root]# echo "cpio -idvF /Disque1/sauvegardes/HomeGroupeA.`date +%j`.cpio" >>CmdSauvegardes
----