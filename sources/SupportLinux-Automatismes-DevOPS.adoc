////
Les supports de Formatux sont publiés sous licence Creative Commons-BY-SA et sous licence Art Libre.
Vous êtes ainsi libre de copier, de diffuser et de transformer librement les œuvres dans le respect des droits de l’auteur.

    BY : Paternité. Vous devez citer le nom de l’auteur original.
    SA : Partage des Conditions Initiales à l’Identique.

Licence Creative Commons-BY-SA : https://creativecommons.org/licenses/by-sa/3.0/fr/
Licence Art Libre : http://artlibre.org/

Auteurs : Patrick Finet, Xavier Sauvignon, Antoine Le Morvan
////
= FORMATUX - Support de cours GNU/Linux : Automatisation - DevOPS
:doctype: book
:toc!:
:docinfo:
:encoding: utf-8
:lang: fr
:numbered:
:sectnumlevels: 2
:description: Support de cours Linux de Formatux - Automatisation - DevOPS
:revnumber: 1.2
:revdate: 23-08-2017
:revremark: Version 1.2
:version-label!:
:experimental:
:source-highlighter: coderay
:coderay-linenums-mode: table
:checkedbox: pass:normal[+&#10004;]+]

:numbered:
include::0000-preface.adoc[]

=== Gestion des versions

.Historique des versions du document
[width="100%",options="header",cols="1,2,4"]
|====================
| Version | Date | Observations
| 1.0 | Avril 2017 | Version initiale.
| 1.1 | Juillet 2017 | Ajout de généralités.
| 1.2 | Aout 2017 | Ajout du cours Jenkins et Rundeck
| 1.3 | Février 2019 | Ajout des cours Ansible Niveau 2, Ansistrano, Asciidoc, Terraform
|====================

:numbered!:

include::DEVOPS-000-Devops.adoc[leveloffset=+1]

include::DEVOPS-010-Puppet.adoc[leveloffset=+1]

include::DEVOPS-020-Ansible-Niveau-1.adoc[leveloffset=+1]

include::DEVOPS-021-Ansible-Niveau-2.adoc[leveloffset=+1]

include::DEVOPS-025-Ansistrano.adoc[leveloffset=+1]

include::DEVOPS-030-Rundeck.adoc[leveloffset=+1]

include::DEVOPS-040-Jenkins.adoc[leveloffset=+1]

include::DEVOPS-050-DocAsCode-Asciidoc.adoc[leveloffset=+1]

include::DEVOPS-060-Terraform.adoc[leveloffset=+1]

include::glossary.adoc[]

[index]
== Index
////////////////////////////////////////////////////////////////
The index is normally left completely empty, it's contents being
generated automatically by the DocBook toolchain.
////////////////////////////////////////////////////////////////
