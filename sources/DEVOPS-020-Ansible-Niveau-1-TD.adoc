////
Les supports de Formatux sont publiés sous licence Creative Commons-BY-SA et sous licence Art Libre.
Vous êtes ainsi libre de copier, de diffuser et de transformer librement les œuvres dans le respect des droits de l’auteur.

    BY : Paternité. Vous devez citer le nom de l’auteur original.
    SA : Partage des Conditions Initiales à l’Identique.

Licence Creative Commons-BY-SA : https://creativecommons.org/licenses/by-sa/3.0/fr/
Licence Art Libre : http://artlibre.org/

Auteurs : Antoine Le Morvan
////
= TP Ansible Base

.icon:mortar-board[] Objectifs
****
icon:square-o[] Mettre en oeuvre Ansible ; +
icon:square-o[] Appliquer des changements de configuration sur un serveur ; +
icon:square-o[] Créer des playbooks Ansible. +
****

:numbered!:
:sectnumlevels!:

== Module 2 : Installation sur le serveur de gestion

****
Objectifs

*	Vérifier l’installation d’ansible sur le serveur
****

=== Exercice 2.1 : Prise en main des VM de TP

Vous allez travailler sur 2 VM :

* La VM de gestion : *server-X*.
** Vous installerez `ansible` sur ce serveur.
** Adresse IP : 10.1.1.X
* La VM cliente : *client-X*.
** Cette VM n'a aucun logiciel spécifique.
** Adresse IP : 10.1.1.X

Pour accéder à ces serveurs, vous passerez par une adresse IP publique et une redirection de port vers votre serveur.

Exemple :

[source]
----
ssh -p 2222 aftec@109.238.4.96 # Acces au serveur
ssh -p 2223 aftec@109.238.4.96 # Acces au client
----

Le mot de passe est `@ns!bl$4@ft$c`

[NOTE]
====
Installez ansible en suivant les instructions fournies à la page suivante : http://docs.ansible.com/ansible/latest/intro_installation.html
====

Uniquement sur le serveur de gestion :

[source]
----
$ sudo yum install -y epel-release # Pour avoir ansible en version 2.7
$ sudo yum install -y ansible
----

=== Exercice 2.2 : Vérification de l’installation

* Vérifier la connectivité entre les serveurs :

Se connecter avec l'utilisateur `aftec` sur le serveur, puis faire un `ping` vers le client :

[source,bash]
----
[aftec] $ ping 10.1.1.11
----

*	Vérifier qu’Ansible est installé sur le serveur

[source,bash]
----
[aftec] $ rpm -qa ansible
ansible-2.7.5-1.el7.noarch
----

*	Vérifier la version d’Ansible :

[source,bash]
----
[aftec] $ ansible --version
ansible 2.7.5
  config file = /etc/ansible/ansible.cfg
  configured module search path = [u'/home/aftec/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python2.7/site-packages/ansible
  executable location = /usr/bin/ansible
  python version = 2.7.5 (default, Jul 13 2018, 13:06:57) [GCC 4.8.5 20150623 (Red Hat 4.8.5-28)]
----

*	Visualiser le fichier de configuration :

[source,bash]
----
~
----

* Visualiser le fichier d’inventaire :

[source,bash]
----
~
----

[NOTE]
====
Plus d'informations sur http://docs.ansible.com/ansible/latest/intro_inventory.html
====

*	Créer deux entrées dans ce fichier inventaire : une votre serveur et une autre pour votre client.

[source,bash]
----
~
~
~
~
~
~
----

* Créer un groupe `tp-ansible` reprenant les deux entrées précédemment créées :

[source,bash]
----
~
~
~
~
----

== Module 3 : Authentification par clef

****
Objectifs

*	Déployer une clef privée/publique sur les clients
****

=== Exercice 3.1 : Créer une bi-clefs

Sur le serveur de gestion :

[source,bash]
----
[aftec] $ ssh-keygen
----

Copier la clef publique sur les 2 VM :

[source,bash]
----
[aftec] $ ssh-copy-id aftec@10.1.1.10
[aftec] $ ssh-copy-id aftec@10.1.1.11
----

Tester une connexion SSH vers chacun des serveurs.

[NOTE]
====
Aucun mot de passe ne doit être demandé :
====

[source,bash]
----
[aftec] $ ssh 10.1.1.10
Last login: Wed Jan  3 15:23:00 2018 from 10.1.1.10
----

puis :

[source,bash]
----
[aftec] $ ssh 10.1.1.11
Last login: Wed Jan  3 15:23:00 2018 from 10.1.1.10
----

=== Exercice 3.2 : Tests d'authentification

* Utiliser le module `ping` pour tester le bon fonctionnement de votre plateforme :

[source,bash]
----
~
----

* Utiliser le module `shell` pour récupérer le `uptime` de vos VM :

[source,bash]
----
~
----

== Module 4 : Utilisation en ligne de commande

****
Objectifs

*	Utiliser ansible en lignes de commandes
****

[NOTE]
====
Plus d'informations sur http://docs.ansible.com/ansible/latest/ansible.html
====

* Lister les hôtes appartenants au groupe `tp-ansible` :

[source,bash]
----
~
----

* Afficher les `facts` pour la VM `ansiblecli` :

[source,bash]
----
~
----

== Module 5 : Les modules

****
Objectifs

*	Utiliser les principaux modules Ansible
* Chercher dans la documentation Ansible
****

[NOTE]
====
Plus d'informations sur http://docs.ansible.com/ansible/modules_by_category.html
====

En utilisant Ansible en ligne de commande, vous allez réaliser les actions suivantes :

* Créer les groupes `rennes`, `lille` et `paris`
* Créer un utilisateur `core`
* Modifier l'utilisateur `core` pour qu'il ait l'UID 10000
* Modifier l'utilisateur `core` pour qu'il soit dans le groupe `rennes`
* Installer le logiciel `tree`
* Stopper le service `crond`
* Désactiver le service `atd`
* créer un fichier vide `/tmp/test` avec les droits 644
* Mettre à jour le système sur la VM cliente
* Redémarrer la VM cliente (une petite recherche s'impose !)

[WARNING]
====
Utilisez les modules appropriés plutôt que le module shell.
Pour rappel, les modules disponibles sont listés à cette adresse : http://docs.ansible.com/ansible/modules_by_category.html
====

[NOTE]
====
Vérifier sur le client que les actions sont bien effectuées en utilisant les commandes que vous avez apprises durant les parcours "Utilisateur Linux" ou "Administrateur Linux".
====

* Créer les groupes `rennes`, `lille` et `paris`

[source,bash]
----
~
~
~
----

[source,bash]
----
~
----

[source,bash]
----
~
----

* Créer un utilisateur `core`

[source,bash]
----
~
~
~
~
~
~
----

* Modifier l'utilisateur `core` pour qu'il ait l'UID 10000

[source,bash]
----
~
~
~
~
~
~
~
----

* Modifier l'utilisateur `core` pour qu'il soit invité dans le groupe `rennes`

[source,bash]
----
~
~
~
~
~
~
~
~
----

* Installer le logiciel `tree`

[source,bash]
----
~
----

* Stopper le service `crond`

[source,bash]
----
~
----

* Désactiver le service `auditd`

[source,bash]
----
~
----

* créer un fichier vide `/tmp/test` avec les droits 644

[source,bash]
----
~
----

* Mettre à jour le système sur la VM cliente

[source,bash]
----
~
----

* Redémarrer la VM cliente

[source,bash]
----
~
----

== Module 6 : Les playbooks

****
Objectifs

*	Ecrire ses premiers playbooks
****

[NOTE]
====
Plus d'informations sur http://docs.ansible.com/ansible/latest/playbooks.html et sur http://docs.ansible.com/ansible/latest/ansible-playbook.html.
====

* Ecrire un playbook simple permettant de mettre à jour une VM CentOS 7 et de la redémarrer

.Le playbook patchmanagement.yml
[source,bash]
----
~
~
~
~
~
~
~
----

[source,bash]
----
~
----


* Ecrire un playbook simple permettant de déployer le compte `supervision` (UID: 5000, groupe: rennes)

.Le playbook supervision.yml
[source,bash]
----
~
~
~
~
~
~
~
~
----

[source,bash]
----
~
----

* Ecrire un playbook permettant de déployer apache/php/mariadb..

.Le playbook lamp.yml
[source,bash]
----
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
~
----

[source,bash]
----
~
----
